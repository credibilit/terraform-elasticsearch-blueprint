variable "account" {
  description = "The AWS account number ID"
}

variable "name" {
  description = "A uniq name for ElastiCache Redis components"
}

variable "instance_type" {
  description = "The instance type of the ElastiCsearch "
  default     = "t2.small.elasticsearch"
}

variable "security_groups" {
  type        = "list"
  description = "List of security groups to add permission"
  default     = []
}

variable "port" {
  description = "The port number on which each of the cache nodes will accept connections. Default: 6379"
  default     = 6379
}

variable "vpc_id" {
  description = "The VPC ID"
}

variable "subnet_ids" {
  type        = "list"
  description = "List of VPC Subnet IDs for the cache subnet group"
}

variable "elasticsearch_version" {
  description = "The version number of the elasticsearch to be used for the ES clusters."
  default     = "6.3"
}

variable "maintenance_window" {
  description = "Specifies the weekly time range for when maintenance on the cache cluster is performed. The format is ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period"
  default     = "sun:04:00-sun:06:00"
}

variable "snapshot_window" {
  description = "The daily time range (in UTC) during which ElastiCache will begin taking a daily snapshot of your cache cluster"
  default     = "06:00-07:00"
}

variable "snapshot_retention_limit" {
  description = "The number of days for which ElastiCache will retain automatic cache cluster snapshots before deleting them"
  default     = 5
}

variable "apply_immediately" {
  description = "Specifies whether any modifications are applied immediately, or during the next maintenance window. Default: true"
  default     = true
}

variable "tags" {
  type    = "map"
  default = {}
}

variable "parameters" {
  type    = "list"
  default = []
}

variable "ebs_enabled" {
  default = true
}

variable "volume_size" {
  default = 20
}

variable "instance_count" {
  default = 1
}

variable "zone_awareness_enabled" {
  default = false
}

variable "indices_query_bool_max_clause_count" {
  type    = "string"
  default = "1024"
}
