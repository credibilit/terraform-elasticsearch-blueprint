resource "aws_security_group" "es" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix = "elasticsearch-es-${var.name}"
  description = "ElasticSearch es ${var.name}"
  vpc_id      = "${var.vpc_id}"

  tags {
    Name = "elasticsearch-es-${var.name}"
  }
}

resource "aws_security_group_rule" "es_egress" {
  security_group_id = "${aws_security_group.es.id}"
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

output "security_group" {
  value = {
    id   = "${aws_security_group.es.id}"
    name = "${aws_security_group.es.name}"
  }
}
