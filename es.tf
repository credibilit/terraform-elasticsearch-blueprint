resource "aws_elasticsearch_domain" "es" {
  domain_name           = "${var.name}"
  elasticsearch_version = "${var.elasticsearch_version}"

  cluster_config {
    instance_type          = "${var.instance_type}"
    zone_awareness_enabled = "${var.zone_awareness_enabled}"
    instance_count         = "${var.instance_count}"
  }

  advanced_options {
    "rest.action.multi.allow_explicit_index" = "true"
    "indices.query.bool.max_clause_count"    = "${var.indices_query_bool_max_clause_count}"
  }

  ebs_options {
    ebs_enabled = "${var.ebs_enabled}"
    volume_size = "${var.volume_size}"
  }

  vpc_options {
    subnet_ids         = ["${var.subnet_ids}"]
    security_group_ids = ["${aws_security_group.es.id}"]
  }

  access_policies = <<CONFIG
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "AWS": "*"
        },
        "Action": "es:*",
        "Resource": "arn:aws:es:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:domain/${var.name}/*"
      }
    ]
  }
CONFIG

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags = "${var.tags}"
}

output "elasticsearch" {
  value = {
    id = "${aws_elasticsearch_domain.es.id}"

    # primary_endpoint_address       = "${aws_elasticsearch_domain.es.primary_endpoint_address}"
    # configuration_endpoint_address = "${aws_elasticsearch_domain.es.configuration_endpoint_address}"
  }
}
