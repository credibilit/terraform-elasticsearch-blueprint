// VPC
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"

  tags {
    Name = "elastisearch-test"
  }
}

// Private subnets
resource "aws_subnet" "search_a" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags {
    Name = "search-a"
  }
}

resource "aws_subnet" "search_b" {
  vpc_id            = "${aws_vpc.default.id}"
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1b"

  tags {
    Name = "search-b"
  }
}

// Variables
variable "account" {}

# // Elastisearch es with HA
# module "test" {
#   source  = "../"
#   account = "${var.account}"
#
#   domain_name           = "es-test-acme"
#   vpc_id                = "${aws_vpc.default.id}"
#   elasticsearch_version = "es2.8"
#   instance_type         = "2.8.24"
#
#   subnet_ids = [
#     "${aws_subnet.search_a.id}",
#     "${aws_subnet.search_b.id}",
#   ]
#
#   tags = {
#     Foo = "bar"
#   }
# }
#
# output "test" {
#   value = "${module.test.elastisearch_replication_group}"
# }

// Elastisearch es single node
module "test_single" {
  source  = "../"
  account = "${var.account}"

  name           = "es-test-acme-sing"
  instance_count = 2

  #maintenance_window         = "sun:05:00-sun:06:00"
  instance_type = "t2.small.elasticsearch"

  #automatic_failover_enabled = false
  elasticsearch_version  = "6.3"
  vpc_id                 = "${aws_vpc.default.id}"
  zone_awareness_enabled = true

  subnet_ids = [
    "${aws_subnet.search_a.id}",
    "${aws_subnet.search_b.id}",
  ]

  tags = {
    Foo = "bar"
  }
}

# output "test_single" {
#   value = "${module.test_single.domain_name}"
# }

