Elastisearch Redis Blueprint
===========================

This Terraform module is a blueprint to elastisearch redis cluster with HA

# Use

To create a bucket with this module you need to insert the following piece of code on your own modules:

```
// Call the module
module "<YOUR_MODULE_NAME>" {
  source  = "git::https://bitbucket.org/credibilit/terraform-elasticsearch-blueprint.gitref=<VERSION>"
  account = "${var.account}"

  name       = "same-name-${var.environment}"
  vpc_id     = "${var.vpc_id}"
  subnet_ids = ["${var.application_subnets}"]

  instance_type         = "${var.elasticsearch["instance_type"]}"
  elasticsearch_version = "${var.elasticsearch["elasticsearch_version"]}"
  ebs_enabled           = "${var.elasticsearch["ebs_enabled"]}"
  volume_size           = "${var.elasticsearch["volume_size"]}"
  tags                  = "${var.elasticsearch_extra_tags}"
  instance_count        = "${var.elasticsearch["instance_count"]}"
  subnet_ids = [
    "${aws_subnet.search_a.id}",
    "${aws_subnet.search_b.id}"
  ]
  tags = {
    Foo = "bar"
  }
}

```

Where `<VERSION>` is the desired version of *this* module. The master branch store the list of versions which can be used. The possible parameters are listed in advance on this document.


## Input parameters

The following parameters are used on this module:

- `account`: The AWS account number ID
- `name`: A uniq name for Elastisearch Redis components
- `vpc_id`: The VPC ID
- `subnet_ids`: List of VPC Subnet IDs for the search subnet group

- `instance_type`: The family of the Elastisearch parameter group
- `security_groups`: List of security groups to add permission
- `port`: The port number on which each of the search nodes will accept connections. Default: 6379
- `instance_count`: The count of nodes in elasticsearch
- `elasticsearch_version`: The version number of the search engine to be used.
- `apply_immediately`: Specifies whether any modifications are applied immediately, or during the next maintenance window. Default: true
- `tags`: Map of tags

## Output parameters

This are the outputs exposed by this module.

- `security_group`
- `test`
- `test_single`
- `elastisearch_subnet_group`
- `elastisearch_parameter_group`
